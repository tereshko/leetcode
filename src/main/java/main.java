public class main
{
    public static void main(String[] args) {
        int n=128;
        boolean isExit = false;
        boolean isPowerTwo = false;
        while(!isExit){
            if(n==1){
                isExit = true;
                isPowerTwo = true;
            }
            if(n==0){
                isExit = true;
            }
            n = division(n);

        }
        System.out.println("Answer: " +isPowerTwo);
    }

    public static int division(int number){
        int x = number;
        int result = 0;
        System.out.println("x%1: " +x%1);
        if ((x % 2 == 0)) {
            result = x /2;
        }
        System.out.println("division: " +result);
        return result;
    }
}
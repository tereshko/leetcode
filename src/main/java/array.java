class Solution {
    public int subarraysDivByK(int[] A, int K) {
        int arrayLength = A.length;

        int count = 0;

        int SUM;
        for (int i = 0; i <= arrayLength - 1; i++) {

            SUM = 0;
            int variableMain = A[i];
            SUM = variableMain;

            for (int z = i; z <= arrayLength - 1; z++) {
                int variableCurrent = A[z];

                if ( i == z ) {
                    //TODO ничего не делать, так как мы должны скипнуть процесс прибавления
                } else {
                    SUM = SUM + variableCurrent;
                }

                if (SUM % K == 0) {
                    count++;
                }
            }
        }
        return count;
    }
}